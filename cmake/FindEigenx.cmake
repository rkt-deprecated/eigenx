# - Try to find Eigenx lib
#
# This module supports requiring a minimum version, e.g. you can do
#   find_package(Eigenx 3.1.2)
# to require version 3.1.2 or newer of Eigenx.
#
# Once done this will define
#
#  EIGENX_FOUND - system has eigen lib with correct version
#  EIGENX_INCLUDE_DIR - the eigen include directory
#  EIGENX_VERSION - eigen version
#
# and the following imported target:
#
#  Eigenx::Eigen - The header-only Eigen library
#
# This module reads hints about search locations from 
# the following environment variables:
#
# EIGENX_ROOT
# EIGENX_ROOT_DIR

# Copyright (c) 2006, 2007 Montel Laurent, <montel@kde.org>
# Copyright (c) 2008, 2009 Gael Guennebaud, <g.gael@free.fr>
# Copyright (c) 2009 Benoit Jacob <jacob.benoit.1@gmail.com>
# Redistribution and use is allowed according to the terms of the 2-clause BSD license.

if(NOT Eigenx_FIND_VERSION)
  if(NOT Eigenx_FIND_VERSION_MAJOR)
    set(Eigenx_FIND_VERSION_MAJOR 2)
  endif(NOT Eigenx_FIND_VERSION_MAJOR)
  if(NOT Eigenx_FIND_VERSION_MINOR)
    set(Eigenx_FIND_VERSION_MINOR 91)
  endif(NOT Eigenx_FIND_VERSION_MINOR)
  if(NOT Eigenx_FIND_VERSION_PATCH)
    set(Eigenx_FIND_VERSION_PATCH 0)
  endif(NOT Eigenx_FIND_VERSION_PATCH)

  set(Eigenx_FIND_VERSION "${Eigenx_FIND_VERSION_MAJOR}.${Eigenx_FIND_VERSION_MINOR}.${Eigenx_FIND_VERSION_PATCH}")
endif(NOT Eigenx_FIND_VERSION)

macro(_eigenx_check_version)
  file(READ "${EIGENX_INCLUDE_DIR}/Eigen/src/Core/util/Macros.h" _eigenx_version_header)

  string(REGEX MATCH "define[ \t]+EIGEN_WORLD_VERSION[ \t]+([0-9]+)" _eigenx_world_version_match "${_eigenx_version_header}")
  set(EIGENX_WORLD_VERSION "${CMAKE_MATCH_1}")
  string(REGEX MATCH "define[ \t]+EIGEN_MAJOR_VERSION[ \t]+([0-9]+)" _eigenx_major_version_match "${_eigenx_version_header}")
  set(EIGENX_MAJOR_VERSION "${CMAKE_MATCH_1}")
  string(REGEX MATCH "define[ \t]+EIGEN_MINOR_VERSION[ \t]+([0-9]+)" _eigenx_minor_version_match "${_eigenx_version_header}")
  set(EIGENX_MINOR_VERSION "${CMAKE_MATCH_1}")

  set(EIGENX_VERSION ${EIGENX_WORLD_VERSION}.${EIGENX_MAJOR_VERSION}.${EIGENX_MINOR_VERSION})
  if(${EIGENX_VERSION} VERSION_LESS ${Eigenx_FIND_VERSION})
    set(EIGENX_VERSION_OK FALSE)
  else(${EIGENX_VERSION} VERSION_LESS ${Eigenx_FIND_VERSION})
    set(EIGENX_VERSION_OK TRUE)
  endif(${EIGENX_VERSION} VERSION_LESS ${Eigenx_FIND_VERSION})

  if(NOT EIGENX_VERSION_OK)

    message(STATUS "Eigenx version ${EIGENX_VERSION} found in ${EIGENX_INCLUDE_DIR}, "
                   "but at least version ${Eigenx_FIND_VERSION} is required")
  endif(NOT EIGENX_VERSION_OK)
endmacro(_eigenx_check_version)

if (EIGENX_INCLUDE_DIR)

  # in cache already
  _eigenx_check_version()
  set(EIGENX_FOUND ${EIGENX_VERSION_OK})

else (EIGENX_INCLUDE_DIR)
  
  # search first if an EigenxConfig.cmake is available in the system,
  # if successful this would set EIGENX_INCLUDE_DIR and the rest of
  # the script will work as usual
  find_package(Eigenx ${Eigenx_FIND_VERSION} NO_MODULE QUIET)

  if(NOT EIGENX_INCLUDE_DIR)
    find_path(EIGENX_INCLUDE_DIR NAMES signature_of_eigenx_matrix_library
        HINTS
        ENV EIGENX_ROOT 
        ENV EIGENX_ROOT_DIR
        PATHS
        ${CMAKE_INSTALL_PREFIX}/include
        ${KDE4_INCLUDE_DIR}
        PATH_SUFFIXES eigenx eigen
      )
  endif(NOT EIGENX_INCLUDE_DIR)

  if(EIGENX_INCLUDE_DIR)
    _eigenx_check_version()
  endif(EIGENX_INCLUDE_DIR)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(Eigenx DEFAULT_MSG EIGENX_INCLUDE_DIR EIGENX_VERSION_OK)

  mark_as_advanced(EIGENX_INCLUDE_DIR)

endif(EIGENX_INCLUDE_DIR)

if(EIGENX_FOUND AND NOT TARGET Eigenx::Eigen)
  add_library(Eigenx::Eigen INTERFACE IMPORTED)
  set_target_properties(Eigenx::Eigen PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${EIGENX_INCLUDE_DIR}")
endif()
